Simple Openstreetmap PBF file format reader. http://wiki.openstreetmap.org/wiki/PBF_Format

[![pipeline status](https://gitlab.com/oleksandromelchuk/rust-osm-reader/badges/master/pipeline.svg)](https://gitlab.com/oleksandromelchuk/rust-osm-reader/commits/master)

This lib uses multithreaded approach for reading data and optimized for speed :D. 

Serial read can be achieved by restricting number of additional concurent threads to 0.

For example usate see src/lib.rs tests.
There is GUI example which can be run using cargo run --release --example paint

DONE:
* reading Nodes, Ways, Relatons, Tags

TODO:
* build index for faster read access.
* filtered read.
* documentation and examples.
* template float and ID types for tunable storage precision.

*** Thanks Gitlab.com team for allowing editing this file online!

Principle #1
User can't catch more than ~5mb of text+vector data from a 4"-6" screen.