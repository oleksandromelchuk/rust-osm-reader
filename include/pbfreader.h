#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <new>

/// Type of object stored in Relation
enum class RelationMemeberType {
  /// relation id is for Node type
  Node,
  /// relation id is for Way type
  Way,
  /// relation id is for Relation type
  Relation,
};

struct String;

template<typename T>
struct Vec;

using CoordType = double;

/// Coordinate tupple
struct Coord {
  /// Latitude
  CoordType lat;
  /// Lontitude
  CoordType lon;
};

using IDType = uint32_t;

/// Lightwait Tag representation which contains indices from it's parent Strings table.
struct Tag {
  /// key string index
  IDType key;
  /// value string index
  IDType val;
};

/// Tags
struct Tags {
  /// String table id used to match tags indexes.
  IDType string_table_id;
  /// list of key-value tags.
  Vec<Tag> tags;
};

/// Node with assosiated tags http://wiki.openstreetmap.org/wiki/Node
struct Node {
  /// Node coordinate
  Coord coord;
  /// List of tags
  Tags tags;
};

/// Way http://wiki.openstreetmap.org/wiki/Way
struct Way {
  /// list of nodes ID
  Vec<IDType> nodes;
  /// Way tags
  Tags tags;
};

/// Relation item
struct RelationMemeber {
  /// ID of relation member
  IDType member_id;
  /// Type of relation member
  RelationMemeberType member_type;
  /// Index of string in parent_relation.tags.string_table_id
  IDType role_id;
};

/// Relation of other osm objects
struct Relation {
  /// Telation tags
  Tags tags;
  /// List of relation items
  Vec<RelationMemeber> members;
};

/// Strings table contain strings used by Tags.
struct Strings {
  /// Strings container
  Vec<String> strings;
};

/// Bounding box
struct LBox {
  /// top left bound
  Coord top_left;
  /// bottom right bound
  Coord bottom_right;
};

/// Coordinates of data in
struct BlobPosition {
  /// strat byte in flile
  uintptr_t start;
  /// blob size
  uintptr_t size;
};

/// General file info
struct PbfInfo {
  /// left right top bottom coordinate in nanodegrees
  LBox bbox;
  /// osm blob position
  BlobPosition position;
};

struct CCallback {
  bool (*node_cb)(Node node);
  bool (*way_cb)(Way way);
  bool (*relation_cb)(Relation relation);
  bool (*strings_cb)(Strings strings);
  bool (*info_cb)(PbfInfo info);
  void (*finish_cb)(String msg);
};

extern "C" {

void rust_read_pbf(const String *fname, uint8_t threads, const CCallback *func);

const T *rust_vec_at(const Vec<T> *a, uintptr_t index);

uintptr_t rust_vec_size(const Vec<T> *a);

} // extern "C"
