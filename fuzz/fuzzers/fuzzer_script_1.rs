#![no_main]
#[macro_use] extern crate libfuzzer_sys;
extern crate pbf_reader;
use pbf_reader::PBFData;
use pbf_reader::read_pbf_data;
use std::sync::mpsc;
use std::vec::Vec;
use std::io::Cursor;

fuzz_target!(|data: &[u8]| {
    let (mut node_tx, node_rx) = mpsc::channel::<PBFData>();
    
	    let mut vec = Vec::<u8>::new();
	    for i in data {
	    	// This is ugly :( help me. 
		    vec.push(i.clone());
	    }
	    
	    let reader = Cursor::new(vec);

        match read_pbf_data(reader, 4, &mut node_tx) 
        {
        Ok(_) => {
        let mut count = 0;
        loop {
            let r = node_rx.recv().unwrap();
            if let PBFData::ParseEnd = r {
                break;
            }
            count = count + 1;
        }
        },
        Err(_) => {}
        }
});
