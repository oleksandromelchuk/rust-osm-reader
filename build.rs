extern crate protobuf_codegen_pure;
extern crate cbindgen;

use std::env;

fn main() {

protobuf_codegen_pure::run(protobuf_codegen_pure::Args {
    out_dir: "src",
    input: &["protoc/fileformat.proto", "protoc/osmformat.proto"],
    includes: &["protoc"],
    customize: protobuf_codegen_pure::Customize {
      ..Default::default()
    },
}).expect("protoc");

    let crate_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    cbindgen::Builder::new()
      .with_crate(crate_dir)
      .generate()
      .expect("Unable to generate bindings")
      .write_to_file("include/pbfreader.h");

}