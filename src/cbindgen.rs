pub use osmtypes::*;


#[repr(C)]
pub struct CCallback {
    node_cb: fn(node: Node)->bool,
    way_cb: fn(way: Way)->bool,
    relation_cb: fn(relation: Relation)->bool,
    strings_cb: fn(strings: Strings)->bool,
    info_cb: fn(info: PbfInfo)->bool,
    finish_cb: fn(msg: String)
}

#[no_mangle]
pub extern "C" fn rust_read_pbf(
    fname: &String,
    threads: u8,
    func: &CCallback,
)
{

}




#[no_mangle]
pub extern "C" fn rust_vec_size<T>(a: &Vec<T>) -> usize {
    a.len()
}

#[no_mangle]
pub extern "C" fn rust_vec_at<T>(a: &Vec<T>, index: usize) -> &T {
    &a[index]
}

#[no_mangle]
pub extern "C" fn c_string(str: &String) -> *const [char] {
    &['a']
}